import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Login from '@/components/Login'
import EditContact from '@/components/EditContact'
import AddContact from '@/components/AddContact'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/Home',
      name: 'Home',
      component: Home
    },
    {
      path: '/',
      name: 'Login',
      component: Login 
    },
    {
      path: '/editContact',
      name: 'EditContact',
      component: EditContact 
    },
    {
      path: '/addContact',
      name: 'AddContact',
      component: AddContact 
    }
  ]
})
