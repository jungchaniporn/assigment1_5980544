import axios from 'axios'

export default {
    name: 'HomePage',
    data () {
      return {
        msg: 'This is Home Page',
        searchInput:'',
        Data:[
          {
          contactID: '',
          firstName: '',
          lastName: '',
          gender: '',
          email: '',
          mobile: '',
          facebook:'',
          imageURL:'',
          }
        ],
        test: '',
      }
    },computed: {
      filteredUsers: function () {
        return this.Data.filter((user)=>{
          // return user.firstName.match(this.search)
          return user.firstName.match(this.searchInput) || user.lastName.match(this.searchInput) 
        })
      }
    },
    methods: {
      search: function(){
        console.log(this.searchInput)
        axios.get('http://localhost:3000/contacts/searchByFirstName/' + this.searchInput)
        .then((res) =>{
          this.Data = res.data
          console.log(this.Data)
        })
      },
      edit(contactID){
        window.location.href = 'http://localhost:8081/#/EditContact?contactID=' + contactID;
      },
      removeContact(contactID){
        axios.delete('http://localhost:3000/contacts/delete/' + contactID)
        .then(() => {
          console.log('Delete UserId: ' + contactID)
        })
        .catch((error) => {
          console.log(error)
        })
        window.location.reload()
      }
    },
    async beforeMount() {
      console.log("before mount")
      let result = await axios.get('http://localhost:3000/contacts/listAll/')
      console.log("data",result)
      this.Data = result.data
    },
    
  }
  