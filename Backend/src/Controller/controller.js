const User = require('../Model/model');
const logger = require('../../config/logger');

// const regex = new Regex(/(a|b)*abb/);


function getMockData() {
  return [
    {
      contactID: 1,
      firstName: 'Neramit',
      lastName: 'Singh',
      gender: 'M',
      email: 'abc@gmail.com',
      mobile: '09098789',
      facebook: 'NeyNey',
      imageURL: 'https://semantic-ui.com/images/avatar2/large/matthew.png',
    },
    {
      contactID: 2,
      firstName: 'Alex',
      lastName: 'Alderson',
      gender: 'M',
      email: 'Boobbii@gmail.com',
      mobile: '00999000',
      facebook: 'Alex Alderson',
      imageURL: 'https://semantic-ui.com/images/avatar2/large/kristy.png',
    }, {
      contactID: 3,
      firstName: 'Julia',
      lastName: 'Edenberk',
      gender: 'M',
      email: 'abc@gmail.com',
      mobile: '0924679857',
      facebook: 'Julia Edenberk',
      imageURL: 'https://semantic-ui.com/images/avatar2/large/molly.png',
    },
    {
      contactID: 4,
      firstName: 'Pattric',
      lastName: 'Nurse',
      gender: 'W',
      email: 'ff@gmail.com',
      mobile: '08088888',
      facebook: 'Pattric Edok',
      imageURL: 'https://semantic-ui.com/images/avatar2/large/elyse.png',
    }, {
      contactID: 5,
      firstName: 'Sethawit',
      lastName: 'Hualan',
      gender: 'M',
      email: 'pooo@gmail.com',
      mobile: '09090999',
      facebook: 'Sethawit Hualan',
      imageURL: 'https://semantic-ui.com/images/avatar/large/steve.jpg',
    },
    {
      contactID: 6,
      firstName: 'Sergiro',
      lastName: 'Aguero',
      gender: 'M',
      email: 'ff@gmail.com',
      mobile: '0818889867',
      facebook: 'Sergiro Aguero',
      imageURL: 'https://semantic-ui.com/images/avatar/large/daniel.jpg',
    },
  ];
}

exports.mockDatabase = (req, res) => {
  User.insertMany(getMockData(), (err, users) => {
    if (err) throw err;
    logger.info(`Inserted ${res.insertedCount} items`);
    res.json(users);
    // console.log(users);
  });
};
exports.addData = (req, res) => {
  User.create(req.body, (err) => {
    if (err) throw err;
    res.end('SAVED!');
  });
};
exports.searchDataByfirstName = (req, res) => {
  // const searchName = new Regex('/\W*('+name+ ')\W*/');
  User.find({ firstName: req.params.firstName }, (err, users) => {
    if (err) throw err;
    res.json(users);
    logger.info(users);
  });
};
exports.searchDataBycontactID = (req, res) => {
  User.find({ contactID: req.params.contactID }, (err, users) => {
    if (err) throw err;
    res.json(users);
    logger.info(users);
  });
};
exports.listAllData = (req, res) => {
  User.find({}, (err, users) => {
    if (err) throw err;
    res.json(users);
    logger.info(users);
  });
};
exports.deleteData = (req, res) => {
  res.setHeader('Content-Type', 'application/xml');
  res.send('Deleting...');
  const query = { contactID: req.params.contactID };
  User.deleteOne(query, (err, users) => {
    if (err) throw err;
    logger.info(`ID:${parseInt(req.params.contactID, 10)} is deleted`);
    logger.info(users);
    res.end('DELETED');
  });
};
exports.updateData = (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  res.send('Updating');
  const myquery = { contactID: req.params.contactID };
  const newValues = req.body;
  User.findOneAndUpdate(myquery, newValues, { new: true }, (err, users) => {
    if (err) throw err;
    logger.info(users);
    res.json(users.body);
  });
};
